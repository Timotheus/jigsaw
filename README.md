![Build Status](https://gitlab.com/pages/jigsaw/badges/master/build.svg)

---

Example [Jigsaw] website using GitLab Pages. Adding comment for build.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yml
image: php:7.1

before_script:
  - curl -sS https://getcomposer.org/installer | php
  - curl -sL https://deb.nodesource.com/setup_6.x | bash -
  - apt-get update -yqq
  - apt-get install zlib1g-dev git nodejs -yqq
  - docker-php-ext-install zip
  - php composer.phar install
  - npm install -g gulp && npm install

cache:
  paths:
    - node_modules/
    - vendor/

pages:
  script:
    - npm run deploy
    - mv build_production/ public/
  artifacts:
    paths:
      - public
  only:
    - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Jigsaw
1. Generate the website: `gulp` or `jigsaw build`
1. Preview your project: `gulp watch` or `jigsaw serve`
1. Add content

Read more at Jigsaw's [documentation].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Also, you will need to either remove the variable `$base` from every views or modify the base entry found in [`config.production.php`](config.production.php):

```php
return [
    ...
    'base' => '/jigsaw',
];
```

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

[ci]: https://about.gitlab.com/gitlab-ci/
[Jigsaw]: http://jigsaw.tighten.co/
[install]: http://jigsaw.tighten.co/docs/installation/
[documentation]: http://jigsaw.tighten.co/docs/installation/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

----

Forked from @haleksandre
